import 'package:trabajo_worlde/trabajo_worlde.dart' as trabajo_worlde;

void main() {
  // String primerTexto = 'juan';
  // String segundoTexto = 'juno';

  // var containerKeys_PrimerTexto = primerTexto.split('').asMap().entries;
  var containerKeys_PrimerTexto = llenarTexto('juan');

  // var containerKeys_SegundoTexto = segundoTexto.split('').asMap().entries;
  var containerKeys_SegundoTexto = llenarTexto('juno');

  var contenedorCoincide =
      siEsCoincidencia(containerKeys_PrimerTexto, containerKeys_SegundoTexto);
  // var contenedorCoincide = containerKeys_PrimerTexto
  //     .map((e) => Prueba(letra: e.value, numero: e.key))
  //     .toSet()
  //     .intersection(containerKeys_SegundoTexto
  //         .map((e) => Prueba(letra: e.value, numero: e.key))
  //         .toSet());

  List<Prueba> contenedorNoCoincide = noEsCoincidencia(
      containerKeys_PrimerTexto,
      containerKeys_SegundoTexto,
      contenedorCoincide);
  // List<Prueba> containerIntersection_Lista = [];
  // for (var elemento in containerKeys_SegundoTexto
  //     .map((e) => Prueba(letra: e.value, numero: e.key))) {
  //   print('elemento $elemento');
  //   containerIntersection_Lista.addAll(containerKeys_PrimerTexto
  //       .map((e) => Prueba(letra: e.value, numero: e.key))
  //       .where((e) {
  //     print(e);
  //     return e.letra == elemento.letra && !contenedorCoincide.contains(e);
  //   }));
  // }

  print('Coincidencias exactas');
  print(contenedorCoincide);

  print('Coincidencias no exactas');
  print(contenedorNoCoincide);
  // faltan las que no coinciden
}

class Prueba {
  final int numero;
  final String letra;

  const Prueba({required this.numero, required this.letra});
  @override
  bool operator ==(other) =>
      other is Prueba && numero == other.numero && letra == other.letra;
  @override
  int get hashCode => numero.hashCode ^ letra.hashCode;
  @override
  toString() => 'P($numero:$letra)';
}

Iterable<Prueba> llenarTexto(String texto) {
  var textoSeparado = texto.split('').asMap();
  var contendorTexto = textoSeparado.entries;
  return contendorTexto.map((e) => Prueba(letra: e.value, numero: e.key));
}

Set<Prueba> siEsCoincidencia(
    Iterable<Prueba> TextoPrimero, Iterable<Prueba> TextoSegundo) {
  return TextoPrimero.toSet().intersection(TextoSegundo.toSet());
}

List<Prueba> noEsCoincidencia(Iterable<Prueba> TextoPrimero,
    Iterable<Prueba> TextoSegundo, Set<Prueba> ContendorCoincidencias) {
  List<Prueba> coinN = [];
  for (var elemento in TextoSegundo) {
    coinN.addAll(TextoPrimero.where((e) {
      print(e);
      return e.letra == elemento.letra && !ContendorCoincidencias.contains(e);
    }));
  }
  return coinN.toSet().toList();
}
