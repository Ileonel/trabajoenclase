import 'package:trabajo_worlde/trabajo_worlde.dart';
import 'package:test/test.dart';
import '../bin/trabajo_worlde.dart';

void main() {
  group('palabra debe...', () {
    test('si es juno', () {
      var primertexto = llenarTexto("juan");
      var segundotexto = llenarTexto("juno");
      expect(siEsCoincidencia(primertexto, segundotexto),
          llenarTexto("ju").toSet());
    });
    test('si es vacio', () {
      var primertexto = llenarTexto("");
      var segundotexto = llenarTexto("");
      expect(
          siEsCoincidencia(primertexto, segundotexto), llenarTexto("").toSet());
    });
  });
  group('coincidencias no exactas para juan', () {
    test('si es josue', () {
      var primertexto = llenarTexto("juan");
      var segundotexto = llenarTexto("josue");
      List<Prueba> x = [];
      x.add(Prueba(numero: 3, letra: "n"));
      x.add(Prueba(numero: 2, letra: "a"));
      expect(
          noEsCoincidencia(primertexto, segundotexto,
              siEsCoincidencia(primertexto, segundotexto)),
          x);
    });
    test('si es leonel', () {
      var primertexto = llenarTexto("juan");
      var segundotexto = llenarTexto("leonel");
      List<Prueba> x = [];
      x.add(Prueba(numero: 2, letra: "a"));
      x.add(Prueba(numero: 3, letra: "n"));
      expect(
          noEsCoincidencia(primertexto, segundotexto,
              siEsCoincidencia(primertexto, segundotexto)),
          x);
    });
  });
}
